<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/22/2018
 * Time: 2:25 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class SecurityDAO
{
    private $servername;
    private $db_name;
    private $username;
    private $password;
    private $conn;

    public function __construct()
    {
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "Steelers3";
        $this->db_name = "sock_db";
    }

    // This function will open a connection to the database
    private function establish_connection()
    {
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->db_name);

        if($this->conn->connect_errno > 0){
            die('Unable to connect to database [' . $this->conn->connect_error . ']');
        }
    }

    public function get_all_users(){
        $this->establish_connection();

        $query = "SELECT * FROM sock_db.user";

        $userList = array();

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc())
        {
            $id = $row["User_ID"];
            $username = $row["Username"];
            $password = $row["Password"];
            $email = $row["Email"];
            $gender = $row["Gender"];
            $firstName = $row["First_Name"];
            $lastName = $row["Last_Name"];
            $privilege = $row["User_Privilege"];
            $user = new UserModel($firstName, $lastName, $email, $gender, $username, $password, $privilege);
            $user->setId($id);
            $userList[] = $user;
        }

        return $userList;
    }


    // Given a username, returns the password
    public function find_password_by_username($u)
    {
        $this->establish_connection();

        $query = "SELECT Password FROM sock_db.user WHERE username='$u'";

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc())
        {
            $password = $row["Password"];
        }

        return $password;
    }

    // Given a user object, add the user's info to the database.
    public function add_user_to_db($user)
    {
        $this->establish_connection();

        $query = "INSERT INTO sock_db.user(Username, Password, Email, Gender, First_Name, Last_Name, User_Privilege) VALUES "
                . "('{$user->getUsername()}', '{$user->getPassword()}', '{$user->getEmail()}', {$user->getGender()}, '{$user->getFirstname()}',"
                ."'{$user->getLastname()}', '{$user->getPrivilege()}')";


        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        if($result)
            return true;
        else
            return false;

    }

    public function get_user_privilege($username)
    {
        $this->establish_connection();

        $query = "SELECT User_Privilege FROM sock_db.user WHERE username='$username'";

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc())
        {
            $privilege = $row["User_Privilege"];
        }

        return $privilege;
    }

    public function get_by_id($user_id)
    {
        $this->establish_connection();

        $query = "SELECT * FROM sock_db.user WHERE User_ID='{$user_id}'";


        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc())
        {
            $id = $row["User_ID"];
            $username = $row["Username"];
            $password = $row["Password"];
            $email = $row["Email"];
            $gender = $row["Gender"];
            $firstName = $row["First_Name"];
            $lastName = $row["Last_Name"];
            $privilege = $row["User_Privilege"];
            $user = new UserModel($firstName, $lastName, $email, $gender, $username, $password, $privilege);
            $user->setId($id);
        }

        return $user;
    }

    public function get_by_username($username)
    {
        $this->establish_connection();

        $query = "SELECT * FROM sock_db.user WHERE Username='{$username}'";


        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc())
        {
            $id = $row["User_ID"];
            $username = $row["Username"];
            $password = $row["Password"];
            $email = $row["Email"];
            $gender = $row["Gender"];
            $firstName = $row["First_Name"];
            $lastName = $row["Last_Name"];
            $privilege = $row["User_Privilege"];
            $user = new UserModel($firstName, $lastName, $email, $gender, $username, $password, $privilege);
            $user->setId($id);
        }

        return $user;
    }

    public function delete_user($user)
    {
        $this->establish_connection();

        $query = "DELETE FROM sock_db.user WHERE Username='{$user->getUsername()}'";

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        if($result)
            return true;
        else
            return false;
    }

    public function update_user($user, $user_id)
    {
        $this->establish_connection();

        $query = "UPDATE sock_db.user SET Username= '{$user->getUsername()}', First_Name = '{$user->getFirstname()}',
                 Last_Name = '{$user->getLastname()}', Email = '{$user->getEmail()}', Gender = {$user->getGender()},
                 User_Privilege = {$user->getPrivilege()} WHERE User_ID='{$user_id}'";


        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . '] in User DAO');
        }

        if($result)
            return true;
        else
            return false;

    }

    public function get_user_addresses($user_id)
    {
        $this->establish_connection();

        $addressList = array();

        $query = "SELECT * FROM sock_db.address WHERE User_ID=$user_id";

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . '] in User DAO');
        }

        while($row = $result->fetch_assoc())
        {
            $id = $row["Address_ID"];
            $line1 = $row["Line1"];
            $line2 = $row["Line2"];
            $city = $row["City"];
            $state = $row["State"];
            $zipcode = $row["Zipcode"];
            $isShipping = $row["isShipping"];
            $isBilling = $row["isBilling"];
            $address = new AddressModel($line1, $line2, $city, $state, $zipcode, $isShipping, $isBilling);
            $address->setId($id);
            $addressList[] = $address;
        }

        return $addressList;

    }

    public function get_user_payments($user_id)
    {
        $this->establish_connection();

        $paymentList = array();

        $query = "SELECT * FROM sock_db.payment WHERE User_ID=$user_id";

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . '] in User DAO');
        }

        while($row = $result->fetch_assoc())
        {
            $id = $row["Payment_ID"];
            $address_id = $row["Address_ID"];
            $card_num = $row["Card_Num"];
            $cardholder_name = $row["Cardholder_Name"];
            $cvv = $row["CVV"];
            $payment= new PaymentModel($address_id, $card_num, $cardholder_name, $cvv);
            $payment->setId($id);
            $paymentList[] = $payment;
        }

        return $paymentList;
    }
}