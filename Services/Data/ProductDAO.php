<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/3/2018
 * Time: 12:35 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class ProductDAO
{
    private $servername;
    private $username;
    private $password;
    private $db_name;
    private $conn;

    public function __construct()
    {
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "Steelers3";
        $this->db_name = "sock_db";
    }

    // This function will open a connection to the database
    private function establish_connection()
    {
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->db_name);

        if($this->conn->connect_errno > 0){
            die('Unable to connect to database [' . $this->conn->connect_error . ']');
        }
    }

    public function get_all_products()
    {
        $productList = array();

        $this->establish_connection();

        $query = "SELECT * FROM product";

        if(!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc()){
            $id = $row["Product_ID"];
            $name = $row["Name"];
            $desc = $row["Description"];
            $price = $row["Price"];
            $image = $row["Image"];
            $company = $row["Company"];
            $color = $row["Color"];
            $product = new ProductModel($id, $name, $desc,  $image, $price, $company, $color);
            $productList[] = $product;
        }

        foreach($productList as $p)
        {
            $query = "SELECT * FROM inventory WHERE PRODUCT_ID= {$p->getId()}";

            if(!$result = $this->conn->query($query)) {
                die('There was an error running the query [' . $this->conn->error . ']');
            }

            while($row = $result->fetch_assoc()){
                $small_quant = $row["Small_quant"];
                $med_quant = $row["Med_quant"];
                $lg_quant = $row["Lg_quant"];
                $xl_quant = $row["Xl_quant"];
                $p->setColorSizeQuantity(array("S"=>$small_quant, "M"=>$med_quant,
                    "L"=>$lg_quant, "XL"=>$xl_quant));
            }
        }

        return $productList;
    }

    public function get_product_by_id($id)
    {

        $this->establish_connection();

        $query = "SELECT * FROM product WHERE Product_ID=" .$id;

        if(!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        $row = $result->fetch_assoc();
        $id = $row["Product_ID"];
        $name = $row["Name"];
        $desc = $row["Description"];
        $price = $row["Price"];
        $image = $row["Image"];
        $company = $row["Company"];
        $color = $row["Color"];
        $product = new ProductModel($id, $name, $desc,  $image, $price, $company, $color);

        $query = "SELECT * FROM inventory WHERE PRODUCT_ID= {$product->getId()}";

        if(!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        $row = $result->fetch_assoc();
        $small_quant = $row["Small_quant"];
        $med_quant = $row["Med_quant"];
        $lg_quant = $row["Lg_quant"];
        $xl_quant = $row["Xl_quant"];
        $product->setColorSizeQuantity(array("S"=>$small_quant, "M"=>$med_quant,
            "L"=>$lg_quant, "XL"=>$xl_quant));

        return $product;
    }


    public function get_products_like_searchterm($searchterm)
    {
        $productList = array();

        $this->establish_connection();

        $query = "SELECT * FROM product WHERE Name LIKE '%$searchterm%'";

        if(!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc()){
            $id = $row["Product_ID"];
            $name = $row["Name"];
            $desc = $row["Description"];
            $price = $row["Price"];
            $image = $row["Image"];
            $company = $row["Company"];
            $color = $row["Color"];
            $product = new ProductModel($id, $name, $desc,  $image, $price, $company, $color);
            $productList[] = $product;
        }

        return $productList;
    }

    public function update_product($product)
        {
            $this->establish_connection();
            $id = $product->getId();
            $query = "UPDATE sock_db.product SET Name = '{$product->getName()}', Description = '{$product->getDescription()}',
                     Price = {$product->getPrice()}, Image = '{$product->getImage()}', Company = '{$product->getCompany()}',
                     Color = '{$product->getColor()}' WHERE Product_ID=" .$id;

            if (!$result = $this->conn->query($query)) {
                die('There was an error running the query [' . $this->conn->error . ']');
            }

            if($result)
                return true;
            else
                return false;

    }

    public function add_product($product)
    {
        $this->establish_connection();

        $query = "INSERT INTO sock_db.product(Name, Description, Price, Image, Company, Color) VALUES"
                ."('{$product->getName()}', '{$product->getDescription()}', {$product->getPrice()}, '{$product->getImage()}',"
                ."'{$product->getCompany()}', '{$product->getColor()}')";


        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        if($result)
            return true;
        else
            return false;
    }

    public function delete_product($product)
    {
        $this->establish_connection();

        $query = "DELETE FROM sock_db.Product WHERE PRODUCT_ID={$product->getId()}";

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        if($result)
            return true;
        else
            return false;
    }

    public function add_to_cart($product_id, $user_id)
    {
        $this->establish_connection();

        $query = "INSERT INTO sock_db.cart(Product_ID, User_ID) VALUES ({$product_id}, {$user_id})";

        if (!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        if($result)
            return true;
        else
            return false;

    }


    public function retrieve_cart_list($user_id)
    {
        $cartList = array();


        $this->establish_connection();

        $query = "SELECT * FROM sock_db.cart INNER JOIN sock_db.product ON cart.Product_ID = product.Product_ID WHERE cart.User_ID=$user_id";

        if(!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        while($row = $result->fetch_assoc()){
            $cart_id = $row["Cart_ID"];
            $product_id = $row["Product_ID"];
            $name = $row["Name"];
            $desc = $row["Description"];
            $price = $row["Price"];
            $image = $row["Image"];
            $company = $row["Company"];
            $color = $row["Color"];
            $product = new ProductModel($product_id, $name, $desc,  $image, $price, $company, $color);
            $cartList[] = new CartModel($cart_id, $product);
        }

        return $cartList;
    }

    public function remove_from_cart($cart_id, $user_id)
    {
        $this->establish_connection();

        $query = "DELETE FROM sock_db.cart WHERE  User_ID=$user_id AND Cart_ID=$cart_id";

        if(!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        if($result)
            return true;
        else
            return false;
    }

    public function remove_all_cart_items_from_cart($user_id)
    {
        $this->establish_connection();

        $query = "DELETE FROM sock_db.cart WHERE User_ID=$user_id";

        if(!$result = $this->conn->query($query)) {
            die('There was an error running the query [' . $this->conn->error . ']');
        }

        if($result)
            return true;
        else
            return false;
    }
}
