<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/3/2018
 * Time: 12:34 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class ProductService
{
    public function retrieve_products()
    {
        $dao = new ProductDAO();
        return $dao->get_all_products();
    }

    public function retrieve_product_by_id($id)
    {
        $dao = new ProductDAO();
        return $dao->get_product_by_id($id);
    }

    public function get_products_like_searchterm($searchterm)
    {
        $dao = new ProductDAO();
        return $dao->get_products_like_searchterm($searchterm);
    }

    public function update_product($product)
    {
        $dao = new ProductDAO();
        return $dao->update_product($product);
    }

    public function add_product($product)
    {
        $dao = new ProductDAO();
        return $dao->add_product($product);
    }

    public function delete_product($product)
    {
        $dao = new ProductDAO();
        return $dao->delete_product($product);
    }

    public function add_to_cart($product_id, $user_id)
    {
        $dao = new ProductDAO();
        return $dao->add_to_cart($product_id, $user_id);
    }

    public function remove_from_cart($cart_id, $user_id)
    {
        $dao = new ProductDAO();
        return $dao->remove_from_cart($cart_id, $user_id);
    }

    public function retrieve_cart_list($user_id)
    {
        $dao = new ProductDAO();
        return $dao->retrieve_cart_list($user_id);
    }

    public function remove_all_cart_items_from_cart($user_id)
    {
        $dao = new ProductDAO();
        return $dao->remove_all_cart_items_from_cart($user_id);
    }

}