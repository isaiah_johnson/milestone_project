<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/12/2018
 * Time: 10:14 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class SecurityService
{
    public function authenticate($username, $password)
    {

        $securityDAO = new SecurityDAO();

        if ($username == "" || $password == "")
        {
            return False;
        }
        else
        {
            // Check to make sure that the user supplied password matches the password in the database.
            $db_password = $securityDAO->find_password_by_username($username);
            if($db_password != null)
            {
                if ($password == $db_password)
                {
                    return True;
                }
                else
                {
                    return False;
                }
            }
            else
            {
                return False;
            }
        }
    }

    public function get_user_privilege($username)
    {
        $securityDAO = new SecurityDAO();
        return ($securityDAO->get_user_privilege($username));
    }

    public function get_all_users(){
        $securityDAO = new SecurityDAO();
        return $securityDAO->get_all_users();
    }

    public function get_by_id($user_id)
    {
        $securityDAO = new SecurityDAO();
        return $securityDAO->get_by_id($user_id);
    }

    public function get_by_username($username)
    {
        $securityDAO = new SecurityDAO();
        return $securityDAO->get_by_username($username);
    }

    public function add_user($user) {
        // Returns true if the user was succesfully added to the database.
        $securityDAO = new SecurityDAO();
        return($securityDAO->add_user_to_db($user));
    }

    public function delete_user($user)
    {
        $securityDAO = new SecurityDAO();
        return $securityDAO->delete_user($user);
    }

    public function update_user($user, $user_id)
    {
        $securityDAO = new SecurityDAO();
        return $securityDAO->update_user($user, $user_id);
    }

    public function get_user_addresses($user_id)
    {
        $securityDAO = new SecurityDAO();
        return $securityDAO->get_user_addresses($user_id);
    }

    public function get_user_payments($user_id){
        $securityDAO = new SecurityDAO();
        return $securityDAO->get_user_payments($user_id);
    }
}

?>