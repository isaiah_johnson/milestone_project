<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/16/2018
 * Time: 8:19 PM
 */


if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(!isset($_SESSION['logged_in']))
{
    $_SESSION['logged_in'] = false;
}