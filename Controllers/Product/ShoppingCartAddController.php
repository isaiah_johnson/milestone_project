<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/9/2018
 * Time: 11:00 AM
 */
require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";

$user_id = $_SESSION["user_id"];
$product_id = $_POST["product_id"];

if((new ProductService())->add_to_cart($product_id, $user_id))
{ echo "Product added to cart!";}
else{echo "Something went wrong adding product to cart!";}
