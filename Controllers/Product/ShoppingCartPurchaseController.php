<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 9:34 PM
 */
require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";

$user_id = $_SESSION["user_id"];

$productService = new ProductService();

if($productService->remove_all_cart_items_from_cart($user_id))
{
    echo "Item purchased successfully";
}
else{
    echo "Something went wrong with item purchase!";
}