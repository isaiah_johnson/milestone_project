<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/2/2018
 * Time: 10:29 AM
 */
require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

$productService = new ProductService();
$product = new ProductModel(0, "", "", "", 0.00, "", "" );

if(isset($_POST["name"])){$product->setName($_POST["name"]);}
if(isset($_POST["description"])){$product->setDescription($_POST["description"]);}
if(isset($_POST["price"])){$product->setPrice($_POST["price"]);}
if(isset($_POST["image"])){$product->setImage($_POST["image"]);}
if(isset($_POST["company"])){$product->setCompany($_POST["company"]);}
if(isset($_POST["color"])){$product->setColor($_POST["color"]);}

$productService->add_product($product);

header("Location: /MilestoneProject/Views/ProductList.php");