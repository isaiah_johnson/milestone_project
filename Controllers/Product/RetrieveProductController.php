<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/3/2018
 * Time: 1:15 AM
 */
require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class RetrieveProductController
{

    public function get_products()
    {
        $productService = new ProductService();

        return $productService->retrieve_products();
    }

    public function get_product_by_id($id)
    {
        $productService = new ProductService();

        return $productService->retrieve_product_by_id($id);
    }

    public function get_products_like_searchterm($searchterm)
    {
        $productService = new ProductService();

        return $productService->get_products_like_searchterm($searchterm);
    }



}