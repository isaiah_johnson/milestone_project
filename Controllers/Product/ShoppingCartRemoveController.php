<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/9/2018
 * Time: 1:11 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";

$user_id = $_SESSION["user_id"];
$cart_id = $_POST["cart_id"];

(new ProductService())->remove_from_cart($cart_id, $user_id);

