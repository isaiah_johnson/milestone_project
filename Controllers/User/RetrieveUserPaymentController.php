<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 3:43 PM
 */

class RetrieveUserPaymentController
{
    public function get_user_payments($user_id)
    {
        $userService = new SecurityService();
        $paymentList = $userService->get_user_payments($user_id);
        return $paymentList;
    }
}