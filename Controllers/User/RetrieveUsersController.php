<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/5/2018
 * Time: 12:25 AM
 */
require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class RetrieveUsersController {

    public function get_all_users()
    {
        $securityService = new SecurityService();
        return $securityService->get_all_users();
    }

    public function get_by_username($username){
        $securityService = new SecurityService();
        return $securityService->get_by_username($username);
    }
}