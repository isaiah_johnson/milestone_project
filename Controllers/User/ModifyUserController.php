<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/7/2018
 * Time: 11:10 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";

$userService = new SecurityService();
$user_id = $_SESSION["user_id"];
$user = $userService->get_by_id($user_id);


if(isset($_POST["username"])){$user->setUsername($_POST["username"]);}
if(isset($_POST["firstname"])){$user->setFirstname($_POST["firstname"]);}
if(isset($_POST["lastname"])){$user->setLastname($_POST["lastname"]);}
if(isset($_POST["email"])){$user->setEmail($_POST["email"]);}
if(isset($_POST["gender"])){$user->setGender($_POST["gender"]);}
if(isset($_POST["privilege"])){$user->setPrivilege($_POST["privilege"]);}


if($userService->update_user($user, $user_id))
{
    $_SESSION["updateSuccess"] = true;
}

header ("Location: ../../Views/UserList.php");