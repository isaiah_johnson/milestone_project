<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/7/2018
 * Time: 10:33 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";

$userService = new SecurityService();

$user = $userService->get_by_username($_POST["username"]);
$_SESSION["username"] = $user->getUsername();


if(isset($_POST["modifyUser"]))
{
    header( "Location: ../../Views/ModifyUser.php");
}
else if(isset($_POST["deleteUser"]))
{
    $userService->delete_user($user);
    header("Location: ../../Views/UserList.php");
}
