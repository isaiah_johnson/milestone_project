<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/22/2018
 * Time: 10:30 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";

$_SESSION['logged_in'] = false;
header("Location: ../../Views/Login.php");
