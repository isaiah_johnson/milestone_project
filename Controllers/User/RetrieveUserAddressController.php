<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 1:42 PM
 */

class RetrieveUserAddressController
{
    public function retrieveUserAddresses($user_id){
        $userService = new SecurityService();
        $addressList = $userService->get_user_addresses($user_id);
        return $addressList;
    }

}