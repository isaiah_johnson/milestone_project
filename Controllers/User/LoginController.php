<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/12/2018
 * Time: 7:27 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";


$username = $_POST["username"];
$password = $_POST["password"];

if($username == NULL || trim($username == ""))
{
    exit("Username is required.");
}
if($password == NULL || trim($password == ""))
{
    exit("Password is required.");
}

// Check to make sure that the username and password are correct.
$service = new SecurityService();
$ok = $service->authenticate($username, $password);


if($ok)
{
    $user = $service->get_by_username($username);

    $_SESSION['logged_in'] = true;
    $_SESSION['privilege'] = $user->getPrivilege();
    $_SESSION['user_id'] = $user->getId();
    header ("Location: ../../Views/Store.php");
}
else
{
    $_SESSION['logged_in'] = false;
    header("Location: ../../Views/LoginFailed.php");
}

?>