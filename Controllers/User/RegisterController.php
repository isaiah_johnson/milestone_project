<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/12/2018
 * Time: 7:27 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$email = $_POST["email"];
$gender = $_POST["radio"];
$username = $_POST["username"];
$password = $_POST["password"];


$user = new UserModel($firstname, $lastname, $email, $gender, $username, $password, 0);
$service = new SecurityService();

$result = $service->add_user($user);

if($result)
    header("Location: ../../Views/Login.php");

?>