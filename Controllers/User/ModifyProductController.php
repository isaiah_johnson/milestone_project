<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/7/2018
 * Time: 9:22 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../../header.php";


$productService = new ProductService();
$product = $productService->retrieve_product_by_id($_POST["id"]);

if(isset($_POST["name"])){$product->setName($_POST["name"]);}
if(isset($_POST["description"])){$product->setDescription($_POST["description"]);}
if(isset($_POST["price"])){$product->setPrice($_POST["price"]);}
if(isset($_POST["image"])){$product->setImage($_POST["image"]);}
if(isset($_POST["company"])){$product->setCompany($_POST["company"]);}
if(isset($_POST["color"])){$product->setColor($_POST["color"]);}



if($productService->update_product($product))
{
    $_SESSION["updateSuccess"] = true;
}

header("Location: ../../Views/ProductList.php");