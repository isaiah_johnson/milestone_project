<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/16/2018
 * Time: 8:21 PM
 */

spl_autoload_register(
    function($className) {
        // echo "register: " . $className . "<br>\n";
        //$fileName = __DIR__ . '/' .  str_replace('\\', '/', $className) . ".php";
        $fileNames = array( __DIR__ . '\\' .  $className . ".php", __DIR__ . '\\' . 'Controllers' . '\\' . $className . ".php",
            __DIR__ . '\\' . 'Controllers' . '\\' .'User' .'\\'  .  $className . ".php", __DIR__ . '\\' . 'Controllers' . '\\' .'Product' .'\\'  .  $className . ".php",
            __DIR__ . '\\' . 'Services' . '\\' . 'Business' . '\\' .  $className . ".php",
            __DIR__ . '\\' . 'Services' . '\\' . 'Data' . '\\' .  $className . ".php",  __DIR__ . '\\' . 'Models' . '\\' .  $className . ".php",
            __DIR__ . '\\' . 'Views' . '\\' .  $className . ".php", __DIR__ . '\\' .'Views' .'\\' .'DynamicRendering' .'\\' . $className . ".php");

        foreach ($fileNames as $fileName)
        {
            if(file_exists($fileName))
            {
                require_once($fileName);
            }
        }
    }
);