<!DOCTYPE html>
<html lang="en">
<?php

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

$product_id = $_GET['productID'];
$prodController = new RetrieveProductController();
$product = $prodController->get_product_by_id($product_id);
?>

<?php DynamicRenderer::generate_head_tags($product->getName());?>

<body>

<?php NavbarGenerator:: render_navbar(); ?>

<div class="container" style="height:100%">
        <div class="row" style="height:100%">
            <div class="col-12 col-sm-6 col-md-8 col-lg-8 border-info bg-secondary mt-4"
                 style="border: 1px solid; height:75vh">
                <div class="row product-picture-row border-info bg-info " style="height:70%; border-bottom:1px solid">
                    <?php echo "<img src='" . $product->getImage() . "' style=\"margin:0 auto; min-width: 50%\" />" ?>
                </div>
                <div class="row product-detail-row" style="height:30%">
                    <div class="col-12">
                        <div class="row" style="height:30%">
                            <h4 class="text-center text-info" style="width:100%"><?php echo $product->getName() ?></h4>
                        </div>
                        <div class="row" style="height:75%">
                            <text class="text-left text-info" style="width:100%">
                                <?php echo "{$product->getDescription()}"?>
                            </text>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 border-info bg-secondary mt-4"
                 style="border: 1px solid; height:75vh">
                <div class="container h-100">
                    <div class="row">
                        <div class="form-group">
                            <legend class="text-info">Number of socks:</legend>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sockNumber" id="singleSock" value="1" checked>
                                    Single
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="sockNumber" id="pairSocks" value="2">
                                    Pair
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="conditionSelect">Condition:</label>
                            <select class="form-control" id="conditionSelect">
                                <option value="1">New</option>
                                <option value="2">Refurbished</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="sizeSelect">Size:</label>
                                <select class="form-control" id="sizeSelect">
                                    <option value="1">Small<?php echo"({$product->getColorSizeQuantity()['S']} remaining)"?></option>
                                    <option value="2">Medium<?php echo"({$product->getColorSizeQuantity()['M']} remaining)"?></option>
                                    <option value="3">Large<?php echo"({$product->getColorSizeQuantity()['L']} remaining)"?></option>
                                    <option value="4">Extra Large<?php echo"({$product->getColorSizeQuantity()['XL']} remaining)"?></option>
                                </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <legend class="text-info">Quantity:</legend>
                            <input type="number" name="quantity" id="quantity-input"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-info">Add to Cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</body>
</html>