<!DOCTYPE html>
<html lang="en">

<?php require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";?>


<?php DynamicRenderer::generate_head_tags("Login");?>

<body class="bg-dark">
<?php NavbarGenerator:: render_navbar(); ?>

<div class="container-fluid" style="height:100%">
    <div class="row" style="margin: 0 auto; height:100%">
        <div class="col-md-12" style="margin: 0 auto">
            <div class="form-box bg-primary border-info">
                <h2 class= "text-center text-secondary border-bottom border-secondary">Welcome to Snag-a-Sock!</h2>
                <form action="../Controllers/User/LoginController.php" method="POST" >
                    <div class="form-group">
                        <label class="text-secondary">Username: </label><input class="form-control text-info" type="text" name="username" maxlength="35" required/>
                    </div>
                    <div class="form-group">
                        <label class="text-secondary">Password: </label><input class="form-control text-info" type="password" name="password" maxlength="20" required/>
                        <a class="form-text text-dark" href="Register.php">Don't have an account? Register here!</a>
                    </div>
                        <input class="btn-info mt-3" id="form-submit" type="submit" name="Login" value="Login"/>
                </form>
            </div>
        </div>
    </div>
</div>



</body>
</html>