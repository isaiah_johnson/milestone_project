<!DOCTYPE html>
<html lang="en">

<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/29/2018
 * Time: 1:03 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

$prodController = new RetrieveProductController();
$product = null;

if(isset($_POST['id']))
{
    $product = $prodController->get_product_by_id($_POST['id']);
}
?>

<?php DynamicRenderer::generate_head_tags("Add a Product");?>

<body>

<?php NavbarGenerator:: render_navbar(); ?>

<div class="container-fluid" style="height:100%">
    <div class="row" style="margin: 0 auto; height:100%">
        <div class="col-md-12" style="margin: 0 auto">
            <div class="form-box bg-primary border-info">
                <h2 class= "text-center text-secondary border-bottom border-secondary">Add Product</h2>
                <form method="post" action='/MilestoneProject/Controllers/Product/addProductController.php'>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Product Name:</label>
                            <input class="form-control text-info" type="text" name="name"/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Description:</label>
                            <input class="form-control text-info" type="text" name="description"/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Price:</label>
                            <input class="form-control text-info" type="number" name="price" step="0.01"/>
                        </div>
                        <div class="col">
                            <label class="text-secondary">Image:</label>
                            <input class="form-control text-info" type="text" name="image"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-4">
                            <label class="text-secondary">Color:</label>
                            <input class="form-control text-info" type="text" name="color"/>
                        </div>
                        <div class="col mb-4">
                            <label class="text-secondary">Company:</label>
                            <input class="form-control text-info" type="text" name="company"/>
                        </div>
                    </div>
                    <div class="row"><h3>Quantities</h3></div>
                    <div class="row">
                        <label class="text-secondary">S:</label>
                        <input class="form-control text-info" type="number" name="small"/>
                        <label class="text-secondary">M:</label>
                        <input class="form-control text-info" type="number" name="medium"/>
                        <label class="text-secondary">L:</label>
                        <input class="form-control text-info" type="number" name="large"/>
                        <label class="text-secondary">XL:</label>
                        <input class="form-control text-info" type="number" name="xlarge"/>
                    </div>
                    <input class="btn-info mt-3" id="form-submit" type="submit" name="submit" value="Modify"/>
                </form>
            </div>
        </div>
    </div>
</div>


</body>
</html>
