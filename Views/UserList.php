<!DOCTYPE html>
<html lang="en">

<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/29/2018
 * Time: 1:03 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

$userController = new RetrieveUsersController();
$userList = $userController->get_all_users();
?>

<?php DynamicRenderer::generate_head_tags("User List") ?>

<body>

<?php NavbarGenerator:: render_navbar(); ?>

<div class="container" style="height:100%">
    <div class="row" style = "height:100%">
        <div class="col-12 col-lg-12" style="100%">
            <table class="user-table">
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Privilege</th>
                <th></th>
                <th></th>

                <?php
                foreach ($userList as $user)
                {
                    if($user->getGender() == 1){
                        $gender = "Male";}
                    else {
                        $gender = "Female";
                    }
                    if($user->getPrivilege() == 0){
                        $privilege = "Client";}
                    else {
                        $privilege = "Admin";
                    }

                    echo "<tr>"
                        ."<form method='post' action='/MilestoneProject/Controllers/User/UserListController.php'>"
                        ."<td style='width:10%'><input type='hidden' name='username' value='{$user->getUsername()}' readonly/>{$user->getUsername()}</td>"
                        ."<td style='width:15%' name='firstName'>{$user->getFirstName()}</td>"
                        ."<td style='width:15%' name='lastName' >{$user->getLastName()}</td>"
                        ."<td style='width:20%' name='email'>{$user->getEmail()}</td>"
                        ."<td style='width:10%' name='gender'>{$gender}</td>"
                        ."<td style='width:10%' name='privilege'>{$privilege}</td>"
                        ."<td style='width:10%;'><button class='btn btn-info' style='max-width:100%' type='submit' name='modifyUser' value='Modify'>Modify</button></td>"
                        ."<td style='width:10%;'><button class='btn btn-info delete-button' type='submit' name='deleteUser' value='Delete'>Delete</button></td>"
                        ."</form>"
                        ."</tr>";
                }
                ?>
            </table>
        </div>
    </div>
</div>`

<script>
    $( document ).ready(function() {
        var success = '<?php if (isset($_SESSION["updateSuccess"])) {
            if($_SESSION["updateSuccess"]){ echo "true";}
            unset($_SESSION["updateSuccess"]);
        } else {
            echo "false";
        } ?>';

        if(success === "true"){
            alert("Update Successful");
        }
    });
</script>


</body>
</html>
