<!DOCTYPE html>
<html lang="en">

<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/29/2018
 * Time: 1:03 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../header.php";

$userController = new RetrieveUsersController();
$user = null;

if(isset($_SESSION['username']))
{
    $user = $userController->get_by_username($_SESSION["username"]);
    unset($_SESSION["username"]);
}
?>


<?php DynamicRenderer::generate_head_tags("Modify User");?>

<body>

<?php NavbarGenerator:: render_navbar(); ?>

<div class="container-fluid" style="height:100%">
    <div class="row" style="margin: 0 auto; height:100%">
        <div class="col-md-12" style="margin: 0 auto">
            <div class="form-box bg-primary border-info">
                <h2 class= "text-center text-secondary border-bottom border-secondary">Register for an Account</h2>
                <form method="post" action="../Controllers/User/ModifyUserController.php">
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">First Name:</label>
                            <input class="form-control text-info" type="text" name="firstname" <?php echo "value='{$user->getFirstname()}'" ?>/>
                        </div>
                        <div class="col">
                            <label class="text-secondary">Last Name:</label>
                            <input class="form-control text-info" type="text" name="lastname" <?php echo "value='{$user->getLastname()}'" ?>/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Email:</label>
                            <input class="form-control text-info" type="text" name="email" <?php echo "value='{$user->getEmail()}'" ?>/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-4">
                            <label class="text-secondary">Gender:</label>
                            <div class="form-check form-check-inline ml-4">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="1"  <?php if($user->getGender() == 1){ echo "checked=''";}?>>
                                <label class="form-check-label text-secondary <?php if($user->getGender() == 1){ echo"active";}?>" for="inlineRadio1">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="2" <?php if($user->getGender() == 2){ echo "checked=''";}?>>
                                <label class="form-check-label text-secondary <?php if($user->getGender() == 2){ echo"active";}?>" for="inlineRadio2">Female</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Username:</label>
                            <input class="form-control text-info" type="text" name="username" <?php echo "value='{$user->getUsername()}'" ?>/>
                        </div>
                        <div class="col">
                            <label class="text-secondary">Privilege:</label>
                            <select class="form-control text-info" id="privilegeSelect" name="privilege">
                                <option value="0">Client</option>
                                <option value="1">Admin</option>
                            </select>
                        </div>
                    </div>
                    <input class="btn-info mt-3" id="form-submit" type="submit" name="submit" value="Modify"/>
                </form>
            </div>
        </div>
    </div>
</div>


</body>
</html>
