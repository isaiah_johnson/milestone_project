<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/22/2018
 * Time: 10:00 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/header.php";

/** This class was created to allow for dynamic rendering of the navbar by checking
 * various conditions of the current session.*/
class DynamicRenderer
{
    public static function generate_head_tags($title)
    {
        echo "<head>"
            ."<meta charset=\"UTF-8\">"
            ."<title>{$title}</title>"
            ."<link rel=\"stylesheet\" href=\"../css/bootstrap.css\"/>"
            ."<link rel=\"stylesheet\" href=\"../css/styles.css\"/>"
            ."<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\"/>"
            ."<script src=\"../js/popper.min.js\"></script>"
            ."<script src=\"../js/jquery-3.3.1.min.js\"></script>"
            ."<script src=\"../js/bootstrap.bundle.js\"></script>"
            ."</head>";
    }


}