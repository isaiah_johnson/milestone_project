<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 12:21 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/header.php";

// Retrieve proudcucts from the database belonging to the user
$user_id = $_SESSION["user_id"];
$cartList = (new ProductService())->retrieve_cart_list($user_id);

// Create a table in the body of the modal, containing the products in the user's cart
foreach ($cartList as $cart_item)
{
    $product = $cart_item->getProduct();

    echo "<table class='cart-table'>"
        ."<tr class='cart-img-row'><td rowspan='2' style='width:20%'><img src='{$product->getImage()}' style='width:100%; display:block;'/></td>"
        . "<td style='width: 40%'><h3 class='text-center'>{$product->getName()}</h3></td></tr> "
        . "<tr><td style='width;40%'><h3 class='text-center'>\${$product->getPrice()}</h3></td>"
        . "<td style='width: 40%'>"
        . "<form class='remove-product-form'>"
        . "<input type='hidden' name='cart_id' value='{$cart_item->getId()}'/>"
        . "<button class='btn btn-info text-center w-100 delete-button' id='{$cart_item->getId()}'>Remove</button></td></tr> "
        . "</form>"
        . "</table>";
}

?>