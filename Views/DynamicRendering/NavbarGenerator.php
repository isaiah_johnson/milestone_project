<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 11:00 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/header.php";

class NavbarGenerator
{
    public static function render_navbar()
    {
        //Check to see that the user is logged in
        if(isset($_SESSION["logged_in"])) {
            $logged_in = $_SESSION["logged_in"];
        }
        else {$logged_in = false;}

        if(isset($_SESSION["privilege"])){
            $user_privilege = $_SESSION["privilege"];
        }
        else{$user_privilege = 0;}


        if($logged_in == true)
        {
            // If it is a customer, render the customer navbar
            if($user_privilege == 0)
            {
                self::create_customer_navbar();
            }
            // It must be an admin
            else
            {
                self::create_admin_navbar();
            }
        }
        else
        {
            self::create_noUser_navbar();
        }
    }

    public static function create_customer_navbar()
    {
        $root_folder = "/MilestoneProject";

        echo "<nav class='navbar navbar-expand-md bg-primary navbar-dark border-bottom border-info'> "
            ."<div class='container-fluid'><a class='navbar-brand' href='{$root_folder}/Views/Store.php'>Snag-a-Sock</a> <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbar2SupportedContent'> <span class='navbar-toggler-icon'></span> </button> "
            ."<div class='collapse navbar-collapse text-center justify-content-end' id='navbar2SupportedContent'> "
            ."<a class='btn navbar-btn btn-primary ml-2 fa fa-shopping-cart fa-lg' style='color: white;' onclick='openCart()'></a> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white' href='{$root_folder}/Views/Store.php'>Store</a> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white' href=''>Account</a> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white font-weight-bold' href='{$root_folder}/Controllers/User/LogoutController.php'>Logout</a> "
            ."</div>"
            ."</div>"
            ."</nav>";

    }

    public static function create_admin_navbar()
    {
        $root_folder = "/MilestoneProject";

        echo "<nav class='navbar navbar-expand-md bg-primary navbar-dark border-bottom border-info'> "
            ."<div class='container-fluid'><a class='navbar-brand' href='{$root_folder}/Views/Store.php'>Snag-a-Sock</a> <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbar2SupportedContent'> <span class='navbar-toggler-icon'></span> </button> "
            ."<div class='collapse navbar-collapse text-center justify-content-end' id='navbar2SupportedContent'> "
            ."<div class='dropdown'>"
            ."<button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>"
            ."Edit Products </button>"
            ."<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>"
            ."<a class='dropdown-item' href='{$root_folder}/Views/AddProduct.php'>Add Product</a>"
            ."<a class='dropdown-item' href='{$root_folder}/Views/ProductList.php'>View Product List</a>"
            ."</div>"
            ."</div>"
            ."<a class='btn navbar-btn btn-primary ml-2 text-white' href='{$root_folder}/Views/UserList.php'> Manage Users</a> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white' href='{$root_folder}/Views/Store.php'>Store</a> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white font-weight-bold' href='{$root_folder}/Controllers/User/LogoutController.php'>Logout</a> "
            ."</div>"
            ."</div>"
            ."</nav>";
    }

    public static function create_noUser_navbar()
    {
        $root_folder = "/MilestoneProject";

        echo "<nav class='navbar navbar-expand-md bg-primary navbar-dark border-bottom border-info'> "
            ."<div class='container-fluid'><a class='navbar-brand' href='{$root_folder}/Views/Store.php'>Snag-a-Sock</a> <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbar2SupportedContent'> <span class='navbar-toggler-icon'></span> </button> "
            ."<div class='collapse navbar-collapse text-center justify-content-end' id='navbar2SupportedContent'> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white font-weight-bold' href='{$root_folder}/Views/Register.php '>Register</a> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white font-weight-bold' href='{$root_folder}/Views/Login.php'>Login</a> "
            ."<a class='btn navbar-btn btn-primary ml-2 text-white' href='{$root_folder}/Views/Store.php'>Store</a> "
            ."</div>"
            ."</div>"
            ."</nav>";

    }
}