<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/9/2018
 * Time: 1:00 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/header.php";

?>

<div class='notification-box'>
     <h5 class='text-center text-white' id='notification-text'></h5>
</div>

<div id='shoppingCartBackground' class='shoppingCartBackground m-0 p-0'>
</div>

<div class='container shopping-cart-container' id='cart-container'>
    <div class='row h-100'>

        <div class='col-8 p-0 m-0'>
            <div id='shoppingCart' class='shopping-cart bg-secondary'>
                <div id='shoppingCartHeader' class='w-100' style='border-bottom:1px solid black'>
                    <h3 class='w-100' style='text-align: center'>Shopping Cart</h3>
                </div>

                <div class='container-fluid' style='height:80%' id='shoppingCartBody'>
                </div>

                <div id='shoppingCartFooter' class='w-100 p-1' style='border-top: 1px solid black; bottom: 0; position: absolute;'>
                    <div class='container h-100 w-100 d-flex align-items-center justify-content-end'>
                        <button id='cartCloseButton' class='btn btn-primary'>Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class='col-4 p-0 m-0 bg-light' id='payment-box'>
            <div id='paymentBoxHeader' class='w-100' style='border-bottom:1px solid black'>
                <h3 class='w-100' style='text-align: center' id='purchase-button'>Purchase</h3>
            </div>

            <div class='container' id='paymentBoxBody'>
            </div>

            <div id='paymentBoxFooter' class='w-100 p-1' style='border-top: 1px solid black; bottom: 0; position: absolute;'>
                <div class='container h-100 w-100 d-flex align-items-center justify-content-end'>
                    <button id='purchaseButton' class='btn btn-info ml-2'>Purchase</button>
                </div>
            </div>
        </div>

    </div>
</div>



<script type=text/javascript>
    $(document).ready(function(){
        generateTable();
        generatePaymentForm();
    });

    function generatePaymentForm(){
        $('#paymentBoxBody').load('DynamicRendering/PaymentBoxFormGenerator.php', function(){});
    }

    function generateTable(){
        $('#shoppingCartBody').load('DynamicRendering/CartItemTableGenerator.php', function(){
        });
    }

    $(document).on('submit', '.remove-product-form', function(e){
        e.preventDefault();
        $.ajax({
            url: '../Controllers/Product/ShoppingCartRemoveController.php',
            type:'POST',
            datatype: 'html',
            data: $(this).serialize(),
            success: function(data){generateTable()}
            });
    });

    $(document).on('submit', '.addToCartForm', function(e){
        e.preventDefault();
        $.ajax({
        url: '../Controllers/Product/ShoppingCartAddController.php',
        type:'POST',
        datatype: 'html',
        data: $(this).serialize(),
        success: function(data){openNotification(data), generateTable()}
        });
    });

    function openNotification(result){
        $('#notification-text').text(result);
        $('.notification-box').fadeIn(1500, function(){
        $(this).fadeOut(1500)});
    }

    $(document).on('click', '#cartCloseButton', function(){
        document.getElementById('shoppingCartBackground').style.display='none';
        $('#cart-container').css({'display': 'none', 'height': '0vh'});
    });

    function openCart(){
        document.getElementById('shoppingCartBackground').style.display='block';
        $('#cart-container').css({'display': 'block', 'height': '90vh', 'transition': 'height 250ms'});
    }
</script>