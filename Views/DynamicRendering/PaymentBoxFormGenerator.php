<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 12:47 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/header.php";


// Retrieve address and payment info from the database given user id
$user_id = $_SESSION["user_id"];
$addressList = (new RetrieveUserAddressController())->retrieveUserAddresses($user_id);
$paymentList = (new RetrieveUserPaymentController())->get_user_payments($user_id);
?>

        <form id="payment-form">
            <div class='row'>
                <div class='form-group m-2 w-100'>
                    <legend class='text-info'>Shipping Address</legend>
                    <?php foreach ($addressList as $address) {
                        echo "<div class='radio'>"
                            . "<label>"
                            . "<input type='radio' name='addressRadio' value='{$address->getId()}' checked>"
                            . "{$address->getLine1()}, {$address->getCity()}, {$address->getState()} {$address->getZipcode()}"
                            . "</label>"
                            . "</div>";
                    }?>
                <button id='addAddress' class='btn btn-info btn-block'>Add Address</button>
                </div>
            </div>

            <div class='row'>
                <div class='form-group m-2 w-100'>
                    <legend class='text-info'>Payment</legend>
                    <?php foreach ($paymentList as $payment) {
                        echo "<div class='radio'>"
                            . "<label>"
                            . "<input type='radio' name='paymentRadio' value='{$payment->getId()}' checked>"
                            . "Card: **** **** **** " . substr($payment->getCardNum(), -4)
                            . "</label>"
                            . "</div>";
                    }?>
                <button id='addPayment' class='btn btn-info btn-block'>Add Payment</button>
                </div>
            </div>
        </form>

<script>

    $(document).ready(function(){
        $('#purchaseButton').click(function(){
            $('#payment-form').submit();
        });
    });

    $(document).on('submit', '#payment-form', function (e) {
        e.preventDefault();
        $.ajax({
            url: '../Controllers/Product/ShoppingCartPurchaseController.php',
            type: 'POST',
            datatype: 'html',
            data: $(this).serialize(),
            success: function (data) {
                openNotification(data);
                generateTable();
            }
        });
    });

</script>