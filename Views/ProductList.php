<!DOCTYPE html>
<html lang="en">

<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/29/2018
 * Time: 1:03 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

$prodController = new RetrieveProductController();

$prodList = $prodController->get_products();

?>

<?php DynamicRenderer::generate_head_tags("Product List");?>

<body>

<?php NavbarGenerator:: render_navbar(); ?>

<div class="container" style="height:100%">
    <div class="row" style = "height:100%">
        <div class="col-12 col-lg-12" style="100%">
            <table class="product-table">
                <th>Product ID</th>
                <th>Product Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Image</th>
                <th>Company</th>
                <th>Color</th>
                <th></th>
                <th></th>

                <?php
                foreach ($prodList as $product)
                {
                    echo "<tr>"
                    ."<form method='post' action='/MilestoneProject/Controllers/Product/ProductListController.php'>"
                    ."<td style='width:5%'><input type='hidden' name='id' value='{$product->getID()}' readonly/>{$product->getID()}</td>"
                    ."<td style='width:10%' name='name'>{$product->getName()}</td>"
                    ."<td style='width:30%' name='description'>{$product->getDescription()}</td>"
                    ."<td style='width:5%' name='price' >\${$product->getPrice()}</td>"
                    ."<td style='width:15%' name='image'>{$product->getImage()}</td>"
                    ."<td style='width:15%' name='company'>{$product->getCompany()}</td>"
                    ."<td style='width:20%' name='color'>{$product->getColor()}</td>"
                    ."<td style='width:10%;'><button class='btn btn-info' type='submit' name='modifyProduct' value='Modify'>Modify</button></td>"
                    ."<td style='width:10%;'><button class='btn btn-info delete-button' type='submit' name='deleteProduct' value='Delete'>Delete</button></td>"
                    ."</form>"
                    ."</tr>";
                }
                ?>
            </table>
        </div>
    </div>
</div>

<script>
    $( document ).ready(function() {
        var success = '<?php if (isset($_SESSION["updateSuccess"])) {
            if($_SESSION["updateSuccess"]){ echo "true";}
            unset($_SESSION["updateSuccess"]);
        } else {
            echo "false";
        } ?>';

        if(success === "true"){
            var notificationBar = document.createElement('span');
            notificationBar.setAttribute('class', 'notification-box');
            notificationBar.innerHTML = 'Update Successful!';
            document.body.appendChild(notificationBar);
        }
    });
</script>


</body>
</html>
