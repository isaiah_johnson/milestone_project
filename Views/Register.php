<!DOCTYPE html>
<html lang="en">

<?php require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php"; ?>


<?php DynamicRenderer::generate_head_tags("Register") ?>

<body class="bg-dark">
<?php NavbarGenerator:: render_navbar(); ?>

<div class="container-fluid" style="height:100%">
    <div class="row" style="margin: 0 auto; height:100%">
        <div class="col-md-12" style="margin: 0 auto">
            <div class="form-box bg-primary border-info">
                <h2 class= "text-center text-secondary border-bottom border-secondary">Register for an Account</h2>
                <form method="post" action="../Controllers/User/RegisterController.php">
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">First Name:</label><input class="form-control text-info" type="text" name="firstname" placeholder="John"/>
                        </div>
                        <div class="col">
                            <label class="text-secondary">Last Name:</label><input class="form-control text-info" type="text" name="lastname" placeholder="Doe"/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Email:</label><input class="form-control text-info" type="text" name="email" placeholder="john.doe@email.com"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-4">
                            <label class="text-secondary">Gender:</label>
                            <div class="form-check form-check-inline ml-4">
                                <input class="form-check-input" type="radio" name="radio" id="inlineRadio1" value="1">
                                <label class="form-check-label text-secondary" for="inlineRadio1">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="radio" id="inlineRadio2" value="2">
                                <label class="form-check-label text-secondary" for="inlineRadio2">Female</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Username:</label><input class="form-control text-info" type="text" name="username" placeholder="johndoe1"/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Password:</label><input class="form-control text-info" onblur="clearPasswordHelp()" onkeypress="passwordHelp()" type="password" id="password" name="password" placeholder="********"/>
                            <small id="passwordHelp" class="form-text text-dark ml-2"></small>
                        </div>
                        <div class="col">
                            <label class="text-secondary">Confirm Password:</label><input class="form-control text-info" type="password" name="passConf" placeholder="********"/>
                        </div>
                    </div>
                    <input class="btn-info mt-3" id="form-submit" type="submit" name="submit" value="Register"/>
                </form>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    function passwordHelp(){
        var field = document.getElementById("password").value;
        var passwordHelp = document.getElementById("passwordHelp");
        if(field.length < 8 || field.length > 20) {
            passwordHelp.innerHTML = "Password must contain 8-20 characters, at least 1 uppercase letter, and 1 number.";
        }
        else
        {
            clearPasswordHelp();
        }
    }
    function clearPasswordHelp() {
        document.getElementById("passwordHelp").innerHTML = "";
    }
</script>

</body>
</html>