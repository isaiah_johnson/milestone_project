<!DOCTYPE html>
<html lang="en">

<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/29/2018
 * Time: 1:03 AM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";
include_once "../header.php";

$prodController = new RetrieveProductController();
$product = null;

if(isset($_SESSION['product_id']))
{
    $product = $prodController->get_product_by_id($_SESSION['product_id']);
    unset($_SESSION["product_id"]);
}
?>

<?php DynamicRenderer::generate_head_tags("Modify Product");?>

<body>

<?php NavbarGenerator:: render_navbar(); ?>

<div class="container-fluid" style="height:100%">
    <div class="row" style="margin: 0 auto; height:100%">
        <div class="col-md-12" style="margin: 0 auto">
            <div class="form-box bg-primary border-info">
                <h2 class= "text-center text-secondary border-bottom border-secondary">Modify Product</h2>
                <form method="post" action='/MilestoneProject/Controllers/Product/ModifyProductController.php'>
                    <div class="row mb-4">
                        <div class="col">
                            <input type='hidden' name='id' <?php echo "value='{$product->getId()}'"?> readonly/>
                            <label class="text-secondary">Product Name:</label>
                            <input class="form-control text-info" type="text" name="name" <?php echo "value='{$product->getName()}'"?>/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Description:</label>
                            <input class="form-control text-info" type="text" name="description" <?php echo "value='{$product->getDescription()}'"?>/>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="text-secondary">Price:</label>
                            <input class="form-control text-info" type="number" name="price" step="0.01" <?php echo "value='{$product->getPrice()}'"?>/>
                        </div>
                        <div class="col">
                            <label class="text-secondary">Image:</label>
                            <input class="form-control text-info" type="text" name="image" <?php echo "value='{$product->getImage()}'"?>/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-4">
                            <label class="text-secondary">Color:</label>
                            <input class="form-control text-info" type="text" name="color"/>
                        </div>
                        <div class="col mb-4">
                            <label class="text-secondary">Company:</label>
                            <input class="form-control text-info" type="text" name="company"/>
                        </div>
                    </div>
                    <input class="btn-info mt-3" id="form-submit" type="submit" name="submit" value="Modify"/>
                </form>
            </div>
        </div>
    </div>
</div>


</body>
</html>
