<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/16/2018
 * Time: 8:54 PM
 */
include_once $_SERVER['DOCUMENT_ROOT']."\MilestoneProject\header.php";

if(isset($_SESSION['principal']) == false || $_SESSION['principal'] == null
|| $_SESSION['principal'] == false)
{
    header("Location: Views/Login.php");
}