<!DOCTYPE html>
<html lang="en">
<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

    $prodController = new RetrieveProductController();
    if(isset($_POST['searchBar']))
    {
        $prodList = $prodController->get_products_like_searchterm($_POST['searchBar']);
    }
    else{
        $prodList = $prodController->get_products();
    }
?>

<?php DynamicRenderer::generate_head_tags("Store") ?>

<body>

<?php NavbarGenerator:: render_navbar(); ?>


<div class="container-fluid bg-secondary border-info" style="height:10%; border-bottom: 1px solid black;">
    <div class="row" style="height:100%;">
        <div class="col-6 col-lg-4 col-md-6 col-sm-6" style="height:100%">
               <form action="Store.php" method="POST" class="row form-inline mt-0 mb-0 pb-0 pt-0" style="height:100%">
                   <div class="col-3 col-lg-2 col-md-2 col-sm-3 ">
                       <label class="text-info" for="searchBar">Search:</label>
                   </div>
                   <div class="col-7 col-lg-8 col-md-8 col-sm-7 ">
                       <input type="text" name="searchBar" class="form-control ml-2 mr-2" id="searchBar" style="width: 100%;">
                   </div>
                   <div class="col-2 col-lg-2 col-md-2 col-sm-2 ">
                       <button type="submit" class="btn btn-dark" style="background-image:url('../img/magnifier.png'); background-size:contain; background-repeat: no-repeat; height:38px; width:38px;">
                       </button>
                   </div>
               </form>
        </div>
        <div class="col-6 col-lg-4 col-md-6 col-sm-6 offset-lg-4">
            <form action="Store.php" method="POST" class="row form-inline mt-0 mb-0 pb-0 pt-0" style="height:100%">
                <div class="col-6 col-lg-6 col-md-6 col-sm-6 ">
                    <label class="text-info float-right" for="sortBar">Sort By:</label>
                </div>
                <div class="col-6 col-lg-6 col-md-6 col-sm-6">
                    <select class="form-control float-right" style="width:100%">
                        <option value="PriceHighToLow" name="sortBar">Price - High to Low</option>
                        <option value="PriceLowToHigh" name="sortBar">Price - Low to High</option>
                        <option value="AToZ" name="sortBar">Alphabetical - A to Z</option>
                        <option value="ZtoA" name="sortBar">Alphabetical - Z to A</option>
                    </select>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="container-fluid" style="height: 100%;">
    <div class="row" style="height:100%">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 border-info" style="border-right:1px solid black;"></div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-10" style="margin-top:2%">
                <div class="row" style="width: 100%; padding-left:1%;">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 d-flex flex-row " style="padding-left: 0;">
                        <h2 class="text-info" align="left">Store</h2>
                    </div>
                </div>
                <div class="row" style="padding-left:1%;">
                    <?php
                            $item_boxes = ceil(count($prodList));

                            for ($j = 0; $j < $item_boxes; $j++)
                            {
                                    echo "<div class=\"item-box border-info bg-secondary\" style='border-bottom: 1px solid black; width: 250px; height:400px'>\n"
                                    ."    <div class=\"row image-row border-info\">\n"
                                    ."        <img src='{$prodList[$j]->getImage()}' style=\"max-width: 100%\" />"
                                    ."    </div>\n"
                                    ."    <div class=\"row item-info-row border-info\">\n"
                                    ."        <div class=\"col-md-8 item-info-col border-info\" style=\"border-right: 2px solid;\">\n"
                                    ."<a href='ProductPage.php?productID={$prodList[$j]->getId()}' class='text-info'>" .$prodList[$j]->getName() ."\n" ."</a>"
                                    ."        </div>\n"
                                    ."        <div class=\"col-md-4 item-info-col border-info\">\n"
                                    ."$" .$prodList[$j]->getPrice() ."\n"
                                    ."        </div>\n"
                                    ."    </div>\n"
                                    ."    <div class=\"row item-select-row\">\n"
                                    ."        <div class=\"col-md-12 item-select-col\">\n"
                                    . "         <form class='addToCartForm' method='post' style='max-height: 100%;'> "
                                    ."            <input type='hidden' name='product_id' value='{$prodList[$j]->getId()}'/> "
                                    ."            <input class=\"btn-info\" id=\"form-submit\" type=\"submit\" value=\"Add To Cart\" style=\"width: 50%; cursor: pointer;\" />\n"
                                    ."         </form>"
                                    ."        </div>\n"
                                    ."    </div>\n"
                                    ."</div>\n";
                            }
                        ?>
                    </div>
            </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $.ajax({
            url: "DynamicRendering/ShoppingCartGenerator.php",
            success: function (data) {
                $('body').append(data);
            },
            dataType: 'html'
        });
    });
</script>
</body>
</html>