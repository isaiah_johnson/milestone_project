<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 1:29 PM
 */

class AddressModel
{
    private $id;
    private $line1;
    private $line2;
    private $city;
    private $state;
    private $zipcode;
    private $isShipping;
    private $isBilling;

    public function __construct($line1, $line2, $city, $state, $zipcode, $isShipping, $isBilling)
    {
        $this->line1 = $line1;
        $this->line2 = $line2;
        $this->city = $city;
        $this->state = $state;
        $this->zipcode = $zipcode;
        $this->isShipping = $isShipping;
        $this->isBilling = $isBilling;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param mixed $line1
     */
    public function setLine1($line1)
    {
        $this->line1 = $line1;
    }

    /**
     * @return mixed
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param mixed $line2
     */
    public function setLine2($line2)
    {
        $this->line2 = $line2;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getisShipping()
    {
        return $this->isShipping;
    }

    /**
     * @param mixed $isShipping
     */
    public function setIsShipping($isShipping)
    {
        $this->isShipping = $isShipping;
    }

    /**
     * @return mixed
     */
    public function getisBilling()
    {
        return $this->isBilling;
    }

    /**
     * @param mixed $isBilling
     */
    public function setIsBilling($isBilling)
    {
        $this->isBilling = $isBilling;
    }


}