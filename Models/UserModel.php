<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 9/22/2018
 * Time: 2:53 PM
 */

require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class UserModel
{
    private $id;
    private $firstname;
    private $lastname;
    private $email;
    private $gender;
    private $username;
    private $password;
    private $privilege;

    public function __construct($f, $l, $e, $g, $u, $p, $priv)
    {
        $this->id = 0;
        $this->firstname = $f;
        $this->lastname = $l;
        $this->email = $e;
        $this->gender = $g;
        $this->username = $u;
        $this->password = $p;
        $this->privilege = $priv;
    }


    // GETTERS AND SETTERS
    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }


    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPrivilege()
    {
        return $this->privilege;
    }

    public function setPrivilege($privilege)
    {
        $this->privilege = $privilege;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

}