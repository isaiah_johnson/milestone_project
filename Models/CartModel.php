<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 8:36 PM
 */

class CartModel
{
    private $id;
    private $product;

    public function __construct($id, $product)
    {
        $this->id = $id;
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product_id
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }


}