<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 11/15/2018
 * Time: 3:37 PM
 */

class PaymentModel
{
    private $id;
    private $address_id;
    private $card_num;
    private $cardholder_name;
    private $cvv;

    public function __construct($address_id, $card_num, $cardholder_name, $cvv)
    {
        $this->address_id = $address_id;
        $this->card_num = $card_num;
        $this->cardholder_name = $cardholder_name;
        $this->cvv = $cvv;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * @param mixed $address_id
     */
    public function setAddressId($address_id)
    {
        $this->address_id = $address_id;
    }

    /**
     * @return mixed
     */
    public function getCardNum()
    {
        return $this->card_num;
    }

    /**
     * @param mixed $card_num
     */
    public function setCardNum($card_num)
    {
        $this->card_num = $card_num;
    }

    /**
     * @return mixed
     */
    public function getCardholderName()
    {
        return $this->cardholder_name;
    }

    /**
     * @param mixed $cardholder_name
     */
    public function setCardholderName($cardholder_name)
    {
        $this->cardholder_name = $cardholder_name;
    }

    /**
     * @return mixed
     */
    public function getCvv()
    {
        return $this->cvv;
    }

    /**
     * @param mixed $cvv
     */
    public function setCvv($cvv)
    {
        $this->cvv = $cvv;
    }


}