<?php
/**
 * Created by PhpStorm.
 * User: IJohnson
 * Date: 10/3/2018
 * Time: 12:29 AM
 */
require_once $_SERVER['DOCUMENT_ROOT']."/MilestoneProject/Autoloader.php";

class ProductModel
{
    private $id;
    private $name;
    private $description;
    private $image;
    private $price;
    private $company;
    private $color;
    private $colorSizeQuantity;

    public function __construct($id, $name, $desc, $image, $price, $company, $color)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $desc;
        $this->image = $image;
        $this->price = $price;
        $this->company = $company;
        $this->color = $color;
    }



    //GETTERS AND SETTERS
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


    public function getPrice()
    {
        return $this->price;
    }


    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function setCompany($company)
    {
        $this->company = $company;
    }

    public function getColorSizeQuantity()
    {
        return $this->colorSizeQuantity;
    }

    public function setColorSizeQuantity($colorSizeQuantity)
    {
        $this->colorSizeQuantity = $colorSizeQuantity;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }


}